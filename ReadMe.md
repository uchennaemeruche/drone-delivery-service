# Drone Delivery Service [Uchenna Emeruche]

### Prerequisites
- Java 11 or higher
- Maven 3.6 or higher
- Docker<Optional> (for running a database in a container)

### Build
To build the project, run the following command in the root directory of the project
``` 
mvn clean install
```
This will compile the code, run tests and package the application in a JAR file.

### Run
To run the application locally, execute the following command:
```
java -jar <path_to_jar_file>
```
The application will start up should be accessible at http://localhost:3000/api/drones

### Test
to run the unit tests, execute the following command:
```
mvn test
```


