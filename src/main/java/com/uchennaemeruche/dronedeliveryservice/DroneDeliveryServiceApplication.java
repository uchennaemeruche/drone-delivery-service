package com.uchennaemeruche.dronedeliveryservice;

import com.uchennaemeruche.dronedeliveryservice.model.Drone;
import com.uchennaemeruche.dronedeliveryservice.model.DroneModel;
import com.uchennaemeruche.dronedeliveryservice.model.DroneState;
import com.uchennaemeruche.dronedeliveryservice.repository.DroneRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DroneDeliveryServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(DroneDeliveryServiceApplication.class, args);
	}


	@Bean
	public CommandLineRunner registerDefaultDrones(DroneRepository droneRepository){
		return args -> {
			Drone drone1 = new Drone();
			drone1.setModel(DroneModel.LIGHTWEIGHT);
			drone1.setState(DroneState.IDLE);
			drone1.setSerialNumber("DR001");
			drone1.setBatteryCapacity(100);

			droneRepository.save(drone1);

			Drone drone2 = new Drone();
			drone2.setModel(DroneModel.CRUISERWEIGHT);
			drone2.setState(DroneState.LOADED);
			drone2.setSerialNumber("DR002");
			drone2.setBatteryCapacity(70);

			droneRepository.save(drone2);
		};
	}
}
