package com.uchennaemeruche.dronedeliveryservice.commons;

import java.util.HashMap;
import java.util.Map;

public class CustomErrorResponse {
    private final Boolean success;
    private final Map<String, Object> error = new HashMap<>();
//    private String message;
//    private String errorTrace;


    public CustomErrorResponse(String message) {
        this.success = false;
        this.error.put("message", message);
//        this.message = message;
//        this.errorTrace = error;

    }

//    public String getMessage() {
//        return this.message;
//    }

//    public void setMessage(String message) {
//        this.message = message;
//    }


//    public String getErrorTrace() {
//        return this.errorTrace;
//    }

//    public void setErrorTrace(String error) {
//        this.errorTrace = error;
//    }

    public Boolean getSuccess() {
        return this.success;
    }

    public Map<String, Object> getError() {
        return this.error;
    }
}
