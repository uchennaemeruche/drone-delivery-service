package com.uchennaemeruche.dronedeliveryservice.commons;

import com.uchennaemeruche.dronedeliveryservice.model.DroneModel;
import jakarta.validation.Constraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;



@Documented
@Constraint(validatedBy = {DroneModelValidator.class})
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DroneModelConstraint {
    String message() default "invalid drone model. it should be one of [Lightweight, Middleweight, Cruiserweight, Heavyweight]";
    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

@Component
class DroneModelValidator implements ConstraintValidator<DroneModelConstraint, DroneModel> {
    @Override
    public boolean isValid(DroneModel value, ConstraintValidatorContext context) {
        if (value == null) {
            return false;
        }
        try {
            DroneModel.valueOfIgnoreCase(value.getModel());
        } catch (IllegalArgumentException ex) {
            return false;
        }
        return true;
    }

}