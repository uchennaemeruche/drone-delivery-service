package com.uchennaemeruche.dronedeliveryservice.commons;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.uchennaemeruche.dronedeliveryservice.model.DroneModel;

import java.io.IOException;

public class DroneModelDeserializer extends JsonDeserializer {
    @Override
    public Object deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
        String input = jsonParser.getValueAsString().toUpperCase();
        return DroneModel.valueOf(input);
    }
}
