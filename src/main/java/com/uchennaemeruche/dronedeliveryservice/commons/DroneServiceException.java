package com.uchennaemeruche.dronedeliveryservice.commons;

public class DroneServiceException extends RuntimeException{

    public DroneServiceException(String message) {
        super(message);
    }

    public static class NotFound extends DroneServiceException {
        public NotFound(String message) {
            super(message);
        }
    }

    public static class DuplicateKey  extends  DroneServiceException{
        public DuplicateKey(String message) {
            super(message == null ? "Possible duplicate entry" : message);
        }
    }

    public static class CapacityExceeded  extends  DroneServiceException{
        public CapacityExceeded(String message) {
            super(message);
        }
    }
    public static class LowBattery extends  DroneServiceException{
        public LowBattery(String message) {
            super(message);
        }
    }

    public static class DroneInUse extends  DroneServiceException{
        public DroneInUse(String message) {
            super(message);
        }
    }
}

