package com.uchennaemeruche.dronedeliveryservice.controller;

import com.uchennaemeruche.dronedeliveryservice.commons.CustomErrorResponse;
import com.uchennaemeruche.dronedeliveryservice.commons.DroneServiceException;
import com.uchennaemeruche.dronedeliveryservice.dto.*;
import com.uchennaemeruche.dronedeliveryservice.model.DroneState;
import com.uchennaemeruche.dronedeliveryservice.service.IDroneService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/drones")
@RequiredArgsConstructor
public class DispatchController {
    private final IDroneService droneService;
    private static final Logger log = LoggerFactory.getLogger(DispatchController.class);

    @PostMapping
    public ResponseEntity<?> registerDrone(@Valid @RequestBody DroneRequestDTO requestBody){
        try {
            DroneDTO result = droneService.registerDrone(requestBody);
            return new ResponseEntity<>(new DroneResponseDTO(result, "Drone registered successfully"), HttpStatus.CREATED);
        }catch(DroneServiceException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomErrorResponse("Drone Operation Error: " + e.getMessage()));
        }catch (Exception e){
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomErrorResponse("An error occurred while registering the Drone"));
        }
    }

    @PostMapping("/load")
    public ResponseEntity<?> loadDrone(@Valid @RequestBody LoadDroneDTO requestBody){
        try {
            droneService.loadDrone(requestBody);
            return new ResponseEntity<>(new DroneResponseDTO(null, "Drone loaded"), HttpStatus.OK);
        }catch (DroneServiceException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomErrorResponse("Drone Operation Error: " + e.getMessage()));
        }catch (Exception e){
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomErrorResponse("An internal server error occurred"));
        }
    }


    @GetMapping("/{serialNumber}/medications")
    public ResponseEntity<?> getLoadedMedicationsForDrone(@PathVariable String serialNumber){
       try{
           List<MedicationResponseDTO> medications = droneService.getLoadedMedicationsForDrone(serialNumber);
           return new ResponseEntity<>(new DroneResponseDTO(medications, "Medications fetched for drone: " + serialNumber), HttpStatus.OK);
       }catch (DroneServiceException e){
           return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomErrorResponse("Drone Operation Error: " + e.getMessage()));
       }catch (Exception e){
           log.error(e.getMessage());
           return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomErrorResponse("An internal server error occurred"));
       }
    }

    @GetMapping
    public ResponseEntity<?> getAvailableDronesForLoading() {
        try {
            List<DroneDTO> droneMedications = droneService.getAvailableDrones(DroneState.IDLE);
            return new ResponseEntity<>(new DroneResponseDTO(droneMedications, "Available drones fetched successfully"), HttpStatus.OK);
        } catch (DroneServiceException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomErrorResponse("Drone Operation Error: " + e.getMessage()));
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomErrorResponse("An internal server error occurred"));
        }
    }

    @GetMapping("{serialNumber}/battery")
    public ResponseEntity<?> checkDroneBatteryLevel(@PathVariable String serialNumber){
        try {
            return new ResponseEntity<>(new DroneResponseDTO(droneService.checkDroneBatteryLevel(serialNumber), "Drone Battery level checked successfully"), HttpStatus.OK);
        }catch (DroneServiceException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new CustomErrorResponse("Drone Operation Error: " + e.getMessage()));
        }catch (Exception e){
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new CustomErrorResponse("An internal server error occurred"));
        }
    }

//    @PutMapping("/{serialNumber}/state/{newState}")
//    public ResponseEntity<Drone> updateDroneState(
//            @PathVariable String serialNumber,
//            @PathVariable DroneState newState) {
//        Drone drone = droneService.updateDroneState(serialNumber, newState);
//        return new ResponseEntity<>(drone, HttpStatus.OK);
//    }


    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Map<String, Object> handleValidationExceptions(MethodArgumentNotValidException ex){
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error ->{
            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName, message);
        });
        Map<String, Object> response = new HashMap<>();
        response.put("success", false);
        response.put("error", errors);
        return response;
    }

}
