package com.uchennaemeruche.dronedeliveryservice.dto;

import com.uchennaemeruche.dronedeliveryservice.model.DroneModel;
import com.uchennaemeruche.dronedeliveryservice.model.DroneState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class DroneDTO {
    private Long id;
    private String serialNumber;

    private DroneModel model;
    private int batteryCapacity;
    private DroneState state;
}
