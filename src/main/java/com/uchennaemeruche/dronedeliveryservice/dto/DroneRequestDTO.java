package com.uchennaemeruche.dronedeliveryservice.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.uchennaemeruche.dronedeliveryservice.commons.DroneModelConstraint;
import com.uchennaemeruche.dronedeliveryservice.commons.DroneModelDeserializer;
import com.uchennaemeruche.dronedeliveryservice.model.DroneModel;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DroneRequestDTO {
    @NotBlank(message = "serial number cannot be empty")
    @Size(max = 100, message = "serial number must not exceed 100 characters")
    private String serialNumber;

//    @NotNull(message = "Drone Model can not be null")
    @DroneModelConstraint
    @JsonDeserialize(using = DroneModelDeserializer.class)
    private DroneModel model;

    @NotNull(message = "Battery capacity cannot be null")
    @Min(value = 0, message = "Battery capacity must be at least 0%")
    @Max(value = 100, message = "Battery capacity cannot exceed 100%")
    private int batteryCapacity;

}
