package com.uchennaemeruche.dronedeliveryservice.dto;

import lombok.Data;

@Data
public class DroneResponseDTO {
    private Boolean success;

    private final Object data;

    private String message;

    public DroneResponseDTO(Object data, String message) {
        this.success = true;
        this.data = data;
        this.message = message;
    }
}
