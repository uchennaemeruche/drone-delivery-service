package com.uchennaemeruche.dronedeliveryservice.dto;

import jakarta.persistence.ElementCollection;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LoadDroneDTO {
    @NotBlank(message = "serial number cannot be empty")
    @Size(max = 100, message = "serial number must not exceed 100 characters")
    private String serialNumber;

    @Valid
    @ElementCollection
    private List<@NotNull MedicationDTO>  medications;
}
