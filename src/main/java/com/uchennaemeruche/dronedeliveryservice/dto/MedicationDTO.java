package com.uchennaemeruche.dronedeliveryservice.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@NoArgsConstructor
@Data
//@Builder
public class MedicationDTO {

    private Long id;
    @NotBlank(message = "Name is required")
    @Pattern(regexp = "^[\\w-]*$", message= "Allowed values for name are letters, numbers, '-','_'")
    private String name;

    @NotNull(message = "Weight is required")
    private double weight;

    @NotBlank(message = "Code is required")
    @Pattern(regexp = "^[A-Z0-9_]*$", message = "Allowed values for code are uppercase letters, underscore, and numbers")
    private String code;
    private MultipartFile image;

    public MedicationDTO(String name, String code, double weight){
        this.name = name;
        this.code = code;
        this.weight = weight;
    }


//    public MedicationDTO(Medication medication){
//        this.id = medication.getId();
//        this.name = medication.getName();
//        this.code = medication.getCode();
//        this.weight = medication.getWeight();
//        this.image = medication.getImage();
//    }
}
