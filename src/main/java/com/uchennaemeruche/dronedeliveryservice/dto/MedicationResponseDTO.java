package com.uchennaemeruche.dronedeliveryservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MedicationResponseDTO {
    private Long id;
    private String name;
    private double weight;

    private String code;
    private String image;

}
