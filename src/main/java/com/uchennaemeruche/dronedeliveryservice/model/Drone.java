package com.uchennaemeruche.dronedeliveryservice.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Table(name = "drones")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Drone {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100, unique = true)
    @NotBlank
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    @NotNull
    private DroneModel model;

    @NotNull
    @Max(100)
    @Min(0)
    private int batteryCapacity;

    @Enumerated(EnumType.STRING)
    @NotNull
    private DroneState state;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Medication> medications;
}
