package com.uchennaemeruche.dronedeliveryservice.model;


public enum DroneModel {
    LIGHTWEIGHT("Lightweight"),
    MIDDLEWEIGHT("Middleweight"),
    CRUISERWEIGHT("Cruiserweight"),
    HEAVYWEIGHT("Heavyweight");

    public static DroneModel valueOfIgnoreCase(String model) {
        return valueOf(model.toUpperCase());
    }
    private final String model;

    DroneModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

}
