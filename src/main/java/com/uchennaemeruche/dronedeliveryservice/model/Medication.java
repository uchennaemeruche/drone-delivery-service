package com.uchennaemeruche.dronedeliveryservice.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "medications")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Pattern(regexp = "^[\\w-]*$", message= "Allowed values for name are letters, numbers, '-','_'")
    private String name;

    @NotNull
    private double weight;

    @NotNull
    @Column(unique = true)
    @Pattern(regexp = "^[A-Z0-9_]*$", message = "Allowed values for code are uppercase letters, underscore, and numbers")
    private String code;

    private String image;

}
