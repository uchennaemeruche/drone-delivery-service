package com.uchennaemeruche.dronedeliveryservice.repository;

import com.uchennaemeruche.dronedeliveryservice.model.DroneBatteryLevelLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneBatteryLevelLogRepository extends JpaRepository<DroneBatteryLevelLog, Long> {
}
