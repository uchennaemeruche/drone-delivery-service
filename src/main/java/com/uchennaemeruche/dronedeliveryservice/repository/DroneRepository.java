package com.uchennaemeruche.dronedeliveryservice.repository;

import com.uchennaemeruche.dronedeliveryservice.model.Drone;
import com.uchennaemeruche.dronedeliveryservice.model.DroneState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {

    Optional<Drone> findBySerialNumber(String serialNumber);

    List<Drone> findByState(DroneState droneState);

    @Query("SELECT d.batteryCapacity FROM Drone d WHERE d.serialNumber = :serialNumber")
    Optional<Integer> findBatteryCapacityBySerialNumber( String serialNumber);
}
