package com.uchennaemeruche.dronedeliveryservice.service;

import com.uchennaemeruche.dronedeliveryservice.dto.DroneDTO;
import com.uchennaemeruche.dronedeliveryservice.model.DroneBatteryLevelLog;
import com.uchennaemeruche.dronedeliveryservice.repository.DroneBatteryLevelLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BatteryLevelScheduler {
    @Autowired
    private DroneServiceImpl droneService;

    @Autowired
    private DroneBatteryLevelLogRepository logRepository;

    private static Logger log = LoggerFactory.getLogger(BatteryLevelScheduler.class);


//    @Scheduled(cron = "0 0 * * * ?") //    Scheduler runs every one hour
    @Scheduled(fixedDelayString = "${fixedRate.in.milliseconds}") // runs every 30 minutes
    public void checkBatteryLevel(){
        List<DroneDTO> drones = droneService.getAllDrones();
        LocalDateTime timestamp = LocalDateTime.now();
        for (DroneDTO drone : drones) {
            int batteryLevel = drone.getBatteryCapacity();
            DroneBatteryLevelLog log = new DroneBatteryLevelLog();
            log.setSerialNumber(drone.getSerialNumber());
            log.setBatteryLevel(batteryLevel);
            log.setTimestamp(timestamp);
            logRepository.save(log);
        }
        log.info("Drone battery level updated at {}", timestamp);
    }
}
