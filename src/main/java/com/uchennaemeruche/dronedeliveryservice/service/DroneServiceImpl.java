package com.uchennaemeruche.dronedeliveryservice.service;

import com.uchennaemeruche.dronedeliveryservice.commons.DroneServiceException;
import com.uchennaemeruche.dronedeliveryservice.dto.*;
import com.uchennaemeruche.dronedeliveryservice.model.Drone;
import com.uchennaemeruche.dronedeliveryservice.model.DroneState;
import com.uchennaemeruche.dronedeliveryservice.model.Medication;
import com.uchennaemeruche.dronedeliveryservice.repository.DroneRepository;
import com.uchennaemeruche.dronedeliveryservice.repository.MedicationRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class DroneServiceImpl implements IDroneService {
    private final DroneRepository droneRepository;

    private final MedicationRepository medicationRepository;

    @Override
    public DroneDTO registerDrone(DroneRequestDTO requestBody) {
        Drone drone = new Drone();
        if(droneRepository.findBySerialNumber(requestBody.getSerialNumber()).isPresent())
            throw new DroneServiceException.DuplicateKey("A drone with this serial number has already been registered");

        drone.setSerialNumber(requestBody.getSerialNumber());
        drone.setModel(requestBody.getModel());
        drone.setState(DroneState.IDLE);
        drone.setBatteryCapacity(requestBody.getBatteryCapacity());
        droneRepository.save(drone);

        return DroneDTO.builder()
                .id(drone.getId())
                .serialNumber(drone.getSerialNumber())
                .batteryCapacity(drone.getBatteryCapacity())
                .model(drone.getModel())
                .state(drone.getState())
                .build();
    }

    @Override
    public void loadDrone(LoadDroneDTO requestBody) {
        final int weightLimit = 500;
//        Check drone availability before loading
//        Check drone battery level
//        Check drone weight and ensure the weight doesn't exceed 500gr

        Drone drone = getDrone(requestBody.getSerialNumber());

        if(drone.getBatteryCapacity() < 25){
            throw new DroneServiceException.LowBattery("Drone battery level is below 25%");
        }
        if(drone.getState() != DroneState.IDLE ) {
            throw new DroneServiceException.DroneInUse("Drone is in use");
        }

        drone.setState(DroneState.LOADING);
        /* TODO
          Send Message to a message broker/Queue to update drone status for other listeners to consume.
        */

//        compute total weight
        double totalWeight = 0;
        List<Medication> medications = new ArrayList<>();
        for (MedicationDTO item : requestBody.getMedications()) {
            Medication medication;

            if( item.getWeight()  == 0 && item.getCode() != null){
                Optional<Medication> optionalMedication = medicationRepository.findByCode(item.getCode());
                if(!optionalMedication.isPresent()){
                    throw new DroneServiceException.NotFound("No Medication found with the given code. kindly provide the name, code, and weight to create one");
                }
                 medication = optionalMedication.get();
                totalWeight += medication.getWeight();
            }else{
                totalWeight += item.getWeight();

                medication = new Medication();
                medication.setCode(item.getCode());
                medication.setName(item.getName());
                medication.setWeight(item.getWeight());

//                Upload image to cloud bucket and store secure url in database table as string.
                String imageUrl = this.uploadImage(item.getImage());
                medication.setImage(imageUrl);

            }
            medications.add(medication);
        }

        if(totalWeight > weightLimit ){
            throw new DroneServiceException.CapacityExceeded(String.format("Total weight of medications(%s)gr exceeds drone capacity of (%s)gr",totalWeight, weightLimit));
        }

//        drone.setBatteryCapacity(drone.getBatteryCapacity() - 25);
        drone.setMedications(medications);
        drone.setState(DroneState.LOADED);
         /* TODO
          Dispatch message to broker/Queue to update drone status for other listeners to consume.
        */
        try {
            droneRepository.save(drone);
        }catch (DataIntegrityViolationException e){
            throw new DroneServiceException.DuplicateKey("Possible duplicate entry");
        }


    }

    public List<MedicationResponseDTO> getLoadedMedicationsForDrone(String serialNumber){
        Drone drone = getDrone(serialNumber);
        List<MedicationResponseDTO>  medications = new ArrayList<>();
        for (Medication medication: drone.getMedications()) {
           medications.add(MedicationResponseDTO.builder()
                   .code(medication.getCode())
                   .id(medication.getId())
                   .name(medication.getName())
                   .weight(medication.getWeight())
                   .image(medication.getImage())
                   .build());
        }
        return medications;
    }

    public List<DroneDTO> getAvailableDrones(DroneState droneState){
        List<Drone> drones = droneRepository.findByState(droneState);
       return drones.stream().map(this::mapDroneToDTO).collect(Collectors.toList());
    }

    @Override
    public Integer checkDroneBatteryLevel(String serialNumber) {
        Optional<Integer> drone =  droneRepository.findBatteryCapacityBySerialNumber(serialNumber);
        if(!drone.isPresent()) throw new DroneServiceException.NotFound("Drone not found");
        return drone.get();
    }

    public List<DroneDTO> getAllDrones(){
        List<Drone> drones = droneRepository.findAll();
        return drones.stream().map(this::mapDroneToDTO).collect(Collectors.toList());
    }

    public Drone getDrone(String serialNumber) {
        Optional<Drone> searchResult = droneRepository.findBySerialNumber(serialNumber);
        if(!searchResult.isPresent()) throw new DroneServiceException.NotFound("Drone not found");
        Drone drone = searchResult.get();
        return drone;
    }

    private DroneDTO mapDroneToDTO(Drone drone) {
       return DroneDTO.builder()
               .serialNumber(drone.getSerialNumber())
               .state(drone.getState())
               .id(drone.getId())
               .batteryCapacity(drone.getBatteryCapacity())
               .model(drone.getModel()).build();
    }

    private String uploadImage(MultipartFile image){
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
       if(image != null && !image.isEmpty()){
//           Upload image to cloudinary or s3 bucket and return secure url
//           return "";
           alphabet = image.getName() + image.getContentType();
       }
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int index = random.nextInt(alphabet.length());
            char randomChar = alphabet.charAt(index);
            stringBuilder.append(randomChar);
        }
        return "https://example.com/drones/uploads/" + stringBuilder.toString() + ".jpg";
    }
}
