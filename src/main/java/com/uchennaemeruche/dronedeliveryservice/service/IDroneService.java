package com.uchennaemeruche.dronedeliveryservice.service;

import com.uchennaemeruche.dronedeliveryservice.dto.DroneDTO;
import com.uchennaemeruche.dronedeliveryservice.dto.DroneRequestDTO;
import com.uchennaemeruche.dronedeliveryservice.dto.LoadDroneDTO;
import com.uchennaemeruche.dronedeliveryservice.dto.MedicationResponseDTO;
import com.uchennaemeruche.dronedeliveryservice.model.DroneState;

import java.util.List;

public interface IDroneService {
    DroneDTO registerDrone(DroneRequestDTO requestBody);
    void loadDrone(LoadDroneDTO requestBody);

    List<MedicationResponseDTO> getLoadedMedicationsForDrone(String serialNumber);
    List<DroneDTO> getAvailableDrones(DroneState droneState);

    Integer checkDroneBatteryLevel(String serialNumber);

    List<DroneDTO> getAllDrones();
}
