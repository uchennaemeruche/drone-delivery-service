package com.uchennaemeruche.dronedeliveryservice;

import com.uchennaemeruche.dronedeliveryservice.dto.DroneRequestDTO;
import com.uchennaemeruche.dronedeliveryservice.model.DroneModel;
import jakarta.validation.ConstraintViolation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class DroneDTOInputValidationTest {
    @Autowired
    LocalValidatorFactoryBean validator;

    @Test
    void test_droneInputWithValidData(){
        DroneRequestDTO inputData = new DroneRequestDTO();
        inputData.setSerialNumber("1234567890123456789012345678901234567890123456789012345678901234");
        inputData.setModel(DroneModel.LIGHTWEIGHT);
        Set<ConstraintViolation<DroneRequestDTO>> violations = validator.validate(inputData);
        assertEquals(0, violations.size());
    }

    void test_droneInputWithInvalidSerialNumber(){
        DroneRequestDTO inputData = new DroneRequestDTO();
        inputData.setSerialNumber("12345678901234567890123456789012345678901234567890123456789012345xywu39292jsjeow83212345678901234567890123456789012345678901234567890123456789012345xywu39292jsjeow832");
        inputData.setModel(DroneModel.LIGHTWEIGHT);
        Set<ConstraintViolation<DroneRequestDTO>> violations = validator.validate(inputData);
        assertEquals(1, violations.size());
        ConstraintViolation<DroneRequestDTO> violation = violations.iterator().next();
        assertEquals("serial number must not exceed 100 characters", violation.getMessage());
    }

    @Test
    void test_droneInputWithInvalidDroneModel(){
        DroneRequestDTO inputData = new DroneRequestDTO();
        inputData.setSerialNumber("abcd1234567890123456789012345678901234567890123456789012345678901234");
        inputData.setModel(null);
        Set<ConstraintViolation<DroneRequestDTO>> violations = validator.validate(inputData);
        assertEquals(1, violations.size());
        ConstraintViolation<DroneRequestDTO> violation = violations.iterator().next();
        assertEquals("invalid drone model. it should be one of [Lightweight, Middleweight, Cruiserweight, Heavyweight]", violation.getMessage());
    }
    @Test()
    void test_valueOfIgnoreCaseWithInvalidValue() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> DroneModel.valueOfIgnoreCase("InvalidValue"));
        assertTrue(exception.getMessage().contains("INVALIDVALUE"));
    }

}
