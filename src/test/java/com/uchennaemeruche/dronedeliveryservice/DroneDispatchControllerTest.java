package com.uchennaemeruche.dronedeliveryservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uchennaemeruche.dronedeliveryservice.commons.DroneServiceException;
import com.uchennaemeruche.dronedeliveryservice.controller.DispatchController;
import com.uchennaemeruche.dronedeliveryservice.dto.*;
import com.uchennaemeruche.dronedeliveryservice.model.DroneModel;
import com.uchennaemeruche.dronedeliveryservice.model.DroneState;
import com.uchennaemeruche.dronedeliveryservice.repository.DroneRepository;
import com.uchennaemeruche.dronedeliveryservice.service.DroneServiceImpl;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
public class DroneDispatchControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DroneServiceImpl droneService;

    @MockBean
    private DroneRepository droneRepository;

    @Autowired
    DispatchController dispatchController;

    private final String testSerialNumber = "23234234";


    @Test
    void test_registerDrone_Success() throws Exception {
//        String requestBody = "{\"serialNumber\": \"123\", \"model\": \"Lightweight\", \"batteryCapacity\": 100}";
        DroneRequestDTO requestDTO = createDroneRequestDTO();

        DroneDTO droneDTO = createExpectedDroneDTOResponse();

        when(droneService.registerDrone(requestDTO)).thenReturn(droneDTO);

         mockMvc.perform(post("/api/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.data.serialNumber").value(testSerialNumber))
                .andExpect(jsonPath("$.data.model").value("LIGHTWEIGHT"))
                 .andExpect(jsonPath("$.message").value("Drone registered successfully"));
    }

    @Test
    void test_registerDrone_Success2() throws Exception {

        DroneRequestDTO requestDTO = createDroneRequestDTO();

        DroneDTO expectedResponse = createExpectedDroneDTOResponse();

        when(droneService.registerDrone(requestDTO)).thenReturn(expectedResponse);

        mockMvc.perform(post("/api/drones")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(requestDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.data.serialNumber", Is.is(testSerialNumber)))
                .andExpect(jsonPath("$.data.model", Is.is("LIGHTWEIGHT")));
    }

    @Test
    void test_registerDrone_ThrowsDuplicateKeyException() throws Exception {
        DroneRequestDTO requestBody = createDroneRequestDTO();

        when(droneService.registerDrone(requestBody)).thenThrow(DroneServiceException.DuplicateKey.class);

        mockMvc.perform(post("/api/drones")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(requestBody)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.success", Is.is(false)))
                .andExpect(jsonPath("$.error.message", containsString("Drone Operation Error")));
        verify(droneService, times(1)).registerDrone(requestBody);
        verifyNoMoreInteractions(droneService);
    }


    @Test
    void test_loadDrone_Success() throws Exception {
       LoadDroneDTO requestBody = createLoadDroneDTO();

       doNothing().when(droneService).loadDrone(requestBody);

       mockMvc.perform(post("/api/drones/load")
               .contentType(MediaType.APPLICATION_JSON)
               .content(new ObjectMapper().writeValueAsString(requestBody)))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.success").value(true))
               .andExpect(jsonPath("$.message").value("Drone loaded"));
        verify(droneService, times(1)).loadDrone(requestBody);
        verifyNoMoreInteractions(droneService);
    }

    @Test
    void test_getLoadedMedicationsFroDrone() throws Exception {
        List<MedicationResponseDTO> medications = new ArrayList<>();
        medications.add(new MedicationResponseDTO());

        when(droneService.getLoadedMedicationsForDrone(testSerialNumber)).thenReturn(medications);

        mockMvc.perform(get("/api/drones/{testSerialNumber}/medications", testSerialNumber))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.message").value("Medications fetched for drone: " + testSerialNumber));

        verify(droneService, times(1)).getLoadedMedicationsForDrone(testSerialNumber);
        verifyNoMoreInteractions(droneService);

    }

    @Test
    void test_GetAvailableDronesForLoading() throws Exception {
        List<DroneDTO> drones = new ArrayList<>();
        drones.add(new DroneDTO());

        when(droneService.getAvailableDrones(DroneState.IDLE)).thenReturn(drones);

        mockMvc.perform(get("/api/drones"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.message").value("Available drones fetched successfully"));

        verify(droneService, times(1)).getAvailableDrones(DroneState.IDLE);
        verifyNoMoreInteractions(droneService);
    }

    @Test
    void test_getDroneBatteryLevel() throws Exception {
        int batteryLevel = 50;
        when(droneService.checkDroneBatteryLevel(testSerialNumber)).thenReturn(batteryLevel);

        mockMvc.perform(get("/api/drones/{serialNumber}/battery", testSerialNumber))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true))
                .andExpect(jsonPath("$.data").value(batteryLevel))
                .andExpect(jsonPath("$.message").value("Drone Battery level checked successfully"));
    }

    public LoadDroneDTO createLoadDroneDTO() {
        LoadDroneDTO requestBody = new LoadDroneDTO();
        requestBody.setSerialNumber("DR001");
        requestBody.setMedications(Arrays.asList(
                new MedicationDTO("Paracetamol", "PARA_001", 100),
                new MedicationDTO("Panadol", "PAN_001", 200)
        ));
        return requestBody;
    }

    private DroneRequestDTO createDroneRequestDTO(){
        DroneRequestDTO requestDTO = new DroneRequestDTO();
        requestDTO.setSerialNumber(testSerialNumber);
        requestDTO.setModel(DroneModel.LIGHTWEIGHT);
        requestDTO.setBatteryCapacity(75);
        return requestDTO;
    }

    private DroneDTO createExpectedDroneDTOResponse(){
        return DroneDTO.builder()
                .serialNumber(testSerialNumber)
                .batteryCapacity(75)
                .model(DroneModel.LIGHTWEIGHT)
                .build();
    }
}
