package com.uchennaemeruche.dronedeliveryservice;

import com.uchennaemeruche.dronedeliveryservice.commons.DroneServiceException;
import com.uchennaemeruche.dronedeliveryservice.dto.DroneDTO;
import com.uchennaemeruche.dronedeliveryservice.dto.DroneRequestDTO;
import com.uchennaemeruche.dronedeliveryservice.dto.MedicationResponseDTO;
import com.uchennaemeruche.dronedeliveryservice.model.Drone;
import com.uchennaemeruche.dronedeliveryservice.model.DroneModel;
import com.uchennaemeruche.dronedeliveryservice.model.DroneState;
import com.uchennaemeruche.dronedeliveryservice.model.Medication;
import com.uchennaemeruche.dronedeliveryservice.repository.DroneRepository;
import com.uchennaemeruche.dronedeliveryservice.service.DroneServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class DroneServiceTest {

    @Mock
    private DroneRepository droneRepository;

    @InjectMocks
    private DroneServiceImpl droneService;

    @Captor
    private ArgumentCaptor<Drone> droneArgumentCaptor;

    private final String testSerialNumber = "111-222-ACD";
    private final int testBatteryCapacity = 75;

    @Test
    void test_registerDrone_withValidRequest_shouldSaveDrone(){

        DroneRequestDTO requestBody = new DroneRequestDTO();
        requestBody.setSerialNumber(testSerialNumber);
        requestBody.setBatteryCapacity(testBatteryCapacity);
        requestBody.setModel(DroneModel.LIGHTWEIGHT);

        Drone drone = new Drone();
        drone.setId(1L);
        drone.setSerialNumber(requestBody.getSerialNumber());
        drone.setBatteryCapacity(requestBody.getBatteryCapacity());
        drone.setModel(requestBody.getModel());
        drone.setState(DroneState.IDLE);

        System.out.println("DRONE:" + drone);

        when(droneRepository.save(any(Drone.class))).thenReturn(drone);

        DroneDTO result = droneService.registerDrone(requestBody);
        System.out.println("RESULT:" + result);

        assertNotNull(result);

        verify(droneRepository, times(1)).save(droneArgumentCaptor.capture());

        Drone captureDrone = droneArgumentCaptor.getValue();

        assertEquals(testSerialNumber, captureDrone.getSerialNumber());
        assertEquals(testBatteryCapacity, captureDrone.getBatteryCapacity());
        assertEquals(DroneModel.LIGHTWEIGHT, captureDrone.getModel());

        assertEquals(testSerialNumber, result.getSerialNumber());
        assertEquals(testBatteryCapacity, result.getBatteryCapacity());
        assertEquals(DroneModel.LIGHTWEIGHT, result.getModel());
    }

    @Test
    void test_registerDrone_withDuplicateSerialNumber_shouldFail(){
        DroneRequestDTO requestBody = new DroneRequestDTO();
        requestBody.setSerialNumber(testSerialNumber);
        requestBody.setBatteryCapacity(testBatteryCapacity);
        requestBody.setModel(DroneModel.CRUISERWEIGHT);

        when(droneRepository.findBySerialNumber(testSerialNumber)).thenReturn(Optional.of(new Drone()));

        assertThrows(DroneServiceException.DuplicateKey.class, () ->{
            droneService.registerDrone(requestBody);
        });
    }

    @Test
    void test_registerDrone_throwsDataViolationException(){
        DroneRequestDTO requestBody = new DroneRequestDTO();
        requestBody.setSerialNumber(testSerialNumber);
        requestBody.setBatteryCapacity(testBatteryCapacity);
        requestBody.setModel(DroneModel.CRUISERWEIGHT);

        when(droneRepository.save(any(Drone.class))).thenThrow(DataIntegrityViolationException.class);

        assertThrows(DataIntegrityViolationException.class, () ->{
            droneService.registerDrone(requestBody);
        });
    }

    @Test
    void test_checkDroneBatteryLevel_Success(){
        Integer expectedBatteryLevel = 80;

        when(droneRepository.findBatteryCapacityBySerialNumber(testSerialNumber)).thenReturn(Optional.of(expectedBatteryLevel));

        // Act
        Integer batteryLevel = droneService.checkDroneBatteryLevel(testSerialNumber);

        // Assert
        assertEquals(expectedBatteryLevel, batteryLevel);
    }

    @Test
    void test_checkDroneBatteryLevel_NotFound(){
        String serialNumber = "12345";
        when(droneRepository.findBatteryCapacityBySerialNumber(testSerialNumber)).thenReturn(Optional.empty());

        assertThrows(DroneServiceException.NotFound.class, () ->{
            droneService.checkDroneBatteryLevel(testSerialNumber);
        });
    }

    @Test
    void test_getAvailableDrones_Success(){
        DroneState droneState = DroneState.IDLE;
        List<Drone> drones = new ArrayList<>();

        drones.add(createDrone("DR001", 99, DroneState.IDLE));
        drones.add(createDrone("DR002", 80, DroneState.LOADED));
        drones.add(createDrone("DR003", 90, DroneState.IDLE));
       drones.add(createDrone("DR004", 40, DroneState.RETURNING));
        drones.add(createDrone("DR005", 50, DroneState.LOADED));
        drones.add(createDrone("DR006", 20, DroneState.DELIVERING));
        drones.add(createDrone("DR007", 90, DroneState.IDLE));
       drones.add(createDrone("DR008", 40, DroneState.DELIVERING));
        drones.add(createDrone("DR009", 0, DroneState.DELIVERED));
        drones.add(createDrone("DR010", 10, DroneState.DELIVERING));

//        Mock repository
        when(droneRepository.findByState(droneState)).thenReturn(drones.stream().filter(drone -> drone.getState() == droneState).collect(Collectors.toList()));

//        Call service method here
        List<DroneDTO> result = droneService.getAvailableDrones(droneState);

        // Verify repository method was called with correct parameter
        verify(droneRepository).findByState(droneState);

//        Verify repository method was called exactly once
        verify(droneRepository, times(1)).findByState(droneState);

        // Verify correct DTOs were returned
        assertEquals(3, result.size());
        assertEquals("DR001", result.get(0).getSerialNumber());
        assertEquals(DroneState.IDLE, result.get(0).getState());
        assertEquals(99, result.get(0).getBatteryCapacity());
        assertEquals("DR007", result.get(2).getSerialNumber());
        assertEquals(DroneState.IDLE, result.get(2).getState());
        assertEquals(90, result.get(1).getBatteryCapacity());
    }

    @Test
    void test_getAvailableDrones_NotFound(){
        DroneState droneState = DroneState.IDLE;

        when(droneRepository.findByState(droneState)).thenReturn(new ArrayList<>());

        List<DroneDTO> result = droneService.getAvailableDrones(droneState);

        verify(droneRepository).findByState(droneState);

        verify(droneRepository, times(1)).findByState(droneState);

        assertEquals(0, result.size());
    }


    @Test
    public void test_getLoadedMedicationsForDrone() {
        final String testSerialNumber = "DR001";

        // create a sample drone with loaded medications
        List<Medication> medications = new ArrayList<>();
        medications.add(createMedication("ABC1", "Medic-1", 100,"https://example.com/image1.png"));
        medications.add(createMedication("ABC2", "Medic-2", 200, "https://example.com/image2.png"));

        Drone drone = createDrone(testSerialNumber,80, DroneState.LOADED);
        drone.setMedications(medications);

        // mock the repository to return the sample drone
        when(droneRepository.findBySerialNumber(testSerialNumber)).thenReturn(Optional.of(drone));

        // call the method and assert the returned list of medications
        List<MedicationResponseDTO> loadedMedications = droneService.getLoadedMedicationsForDrone(testSerialNumber);
        assertEquals(2, loadedMedications.size());

        assertEquals("ABC1", loadedMedications.get(0).getCode());
        assertEquals("Medic-1", loadedMedications.get(0).getName());
        assertEquals(100.0, loadedMedications.get(0).getWeight(), 0.01);
    }


    @Test()
    public void test_getLoadedMedicationsForDrone_NotFound() {
        final String testSerialNumber = "DR001";

        when(droneRepository.findBySerialNumber(testSerialNumber)).thenReturn(Optional.empty());
//
        // call the method and expect a NotFound exception to be thrown
        DroneServiceException.NotFound thrownException = assertThrows(DroneServiceException.NotFound.class, () -> droneService.getLoadedMedicationsForDrone(testSerialNumber));
        assertEquals("Drone not found", thrownException.getMessage());
    }

    @Test
    public void test_getLoadedMedicationsForDrone_ReturnsNoMedications() {
        final String testSerialNumber = "DR001";
        // create a sample drone with no medications
        Drone drone = createDrone(testSerialNumber, 80, DroneState.LOADED);
        drone.setMedications(Collections.emptyList());

        when(droneRepository.findBySerialNumber(testSerialNumber)).thenReturn(Optional.of(drone));

        // call the method and assert that an empty list is returned
        List<MedicationResponseDTO> loadedMedications = droneService.getLoadedMedicationsForDrone(testSerialNumber);
        assertEquals(0, loadedMedications.size());
    }


    private Drone createDrone(String serialNumber, int battery, DroneState state) {
        Drone drone = new Drone();
        drone.setSerialNumber(serialNumber);
        drone.setBatteryCapacity(battery);
        drone.setState(state);

        return drone;
    }
    private Medication createMedication(String code, String name, double weight, String image) {
        Medication medication = new Medication();
        medication.setCode(code);
        medication.setName(name);
        medication.setWeight(weight);
        medication.setImage(image);
        return medication;
    }



}
