package com.uchennaemeruche.dronedeliveryservice;

import com.uchennaemeruche.dronedeliveryservice.commons.DroneServiceException;
import com.uchennaemeruche.dronedeliveryservice.dto.LoadDroneDTO;
import com.uchennaemeruche.dronedeliveryservice.dto.MedicationDTO;
import com.uchennaemeruche.dronedeliveryservice.model.Drone;
import com.uchennaemeruche.dronedeliveryservice.model.DroneState;
import com.uchennaemeruche.dronedeliveryservice.model.Medication;
import com.uchennaemeruche.dronedeliveryservice.repository.DroneRepository;
import com.uchennaemeruche.dronedeliveryservice.repository.MedicationRepository;
import com.uchennaemeruche.dronedeliveryservice.service.DroneServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
public class LoadDroneTest {
    @Mock
    private DroneRepository droneRepository;

    @Mock
    private MedicationRepository medicationRepository;

   @InjectMocks
    private DroneServiceImpl droneService;

   @Test
   void test_loadDrone_withValidRequest_shouldLoadDrone(){
       LoadDroneDTO requestBody  = createLoadDroneDTO();

       Drone drone = createDrone();

       when(droneRepository.findBySerialNumber("DR001")).thenReturn(Optional.of(drone));

       droneService.loadDrone(requestBody);

       verify(droneRepository, times(1)).save(drone);
       assertEquals(DroneState.LOADED, drone.getState());
       assertEquals(2, drone.getMedications().size());
   }

    @Test()
    public void test_loadDrone_shouldThrowLowBatteryException_whenBatteryLevelIsBelow25() {
       // Given
        Drone drone = createDrone();
        drone.setBatteryCapacity(24);

        when(droneRepository.findBySerialNumber("DR001")).thenReturn(Optional.of(drone));

        LoadDroneDTO requestBody = createLoadDroneDTO();

        DroneServiceException.LowBattery thrownEx = assertThrows(DroneServiceException.LowBattery.class, () ->droneService.loadDrone(requestBody));
        assertEquals("Drone battery level is below 25%", thrownEx.getMessage());
    }

    @Test()
    public void test_loadDrone_shouldThrowDroneInUseException_whenDroneStateIsNotIdle() {
       // Given
        Drone drone = createDrone();
        drone.setState(DroneState.DELIVERING);

        when(droneRepository.findBySerialNumber("DR001")).thenReturn(Optional.of(drone));

        LoadDroneDTO requestBody = createLoadDroneDTO();

        // When -> droneService.loadDrone(requestBody);
        // Then throw -> DroneServiceException.DroneInUse

        DroneServiceException.DroneInUse thrownEx = assertThrows(DroneServiceException.DroneInUse.class, () ->droneService.loadDrone(requestBody));
        assertEquals("Drone is in use", thrownEx.getMessage());
    }

    @Test()
    public void test_loadDrone_shouldThrowNotFoundException_whenMedicationCodeIsNotFoundInDBOrNotProvided() {
       // Given
        Drone drone = createDrone();

        when(droneRepository.findBySerialNumber("DR001")).thenReturn(Optional.of(drone));

        when(medicationRepository.findByCode("123")).thenReturn(Optional.empty());

        LoadDroneDTO requestBody = createLoadDroneDTO();

        MedicationDTO medicationDTO = createMedicationDTO();
        requestBody.setMedications(List.of(medicationDTO));

        // When -> droneService.loadDrone(requestBody);
        // Then throw DroneServiceException.NotFound

        DroneServiceException.NotFound thrownEx = assertThrows(DroneServiceException.NotFound.class, () ->droneService.loadDrone(requestBody));
        assertEquals("No Medication found with the given code. kindly provide the name, code, and weight to create one", thrownEx.getMessage());
    }

    @Test()
    public void test_loadDrone_withExceededCapacity_shouldThrowCapacityExceeded() {
       // Arrange
        LoadDroneDTO requestBody = createLoadDroneDTO();
        requestBody.setMedications(Arrays.asList(
                new MedicationDTO("abc123", "Medication 1", 300),
                new MedicationDTO("def456", "Medication 2", 300)
        ));

        Drone drone = createDrone();

        when(droneRepository.findBySerialNumber("DR001")).thenReturn(Optional.of(drone));

        // when -> droneService.loadDrone(requestBody);
        // Then throw DroneServiceException.CapacityExceeded

        DroneServiceException.CapacityExceeded thrownEx = assertThrows(DroneServiceException.CapacityExceeded.class, () ->droneService.loadDrone(requestBody));
        assertEquals("Total weight of medications(600.0)gr exceeds drone capacity of (500)gr", thrownEx.getMessage());
    }

    @Test
    void test_loadDrone_medicationNotFound_throwNotFoundException() {
        // Given
        LoadDroneDTO requestBody = createLoadDroneDTO();
        MedicationDTO medicationDTO = createMedicationDTO();
        medicationDTO.setCode("INVALIDCODE"); // set medication code to invalid value
        requestBody.setMedications(Arrays.asList(medicationDTO));

        Drone drone = createDrone();

        when(droneRepository.findBySerialNumber(anyString())).thenReturn(Optional.of(drone));
        when(medicationRepository.findByCode(anyString())).thenReturn(Optional.empty());

        DroneServiceException.NotFound thrownException = assertThrows(DroneServiceException.NotFound.class, () -> droneService.loadDrone(requestBody));
        assertEquals("No Medication found with the given code. kindly provide the name, code, and weight to create one", thrownException.getMessage());
    }

    @Test
    void test_loadDrone_medicationExistsInDatabaseWithZeroWeight_addToDrone() {
        // Arrange
        LoadDroneDTO requestBody = createLoadDroneDTO();
        MedicationDTO medicationDTO = createMedicationDTO();
        medicationDTO.setWeight(0); // set medication weight to zero
        requestBody.setMedications(Arrays.asList(medicationDTO));

        Medication medication = createMedication();
        medication.setWeight(20); // set medication weight to 20

        Drone drone = createDrone();

        when(droneRepository.findBySerialNumber(anyString())).thenReturn(Optional.of(drone));
        when(medicationRepository.findByCode(anyString())).thenReturn(Optional.of(medication));

        // Do
        droneService.loadDrone(requestBody);

        // Assert
        assertEquals(1, drone.getMedications().size());
        assertEquals(medication.getName(), drone.getMedications().get(0).getName());
        assertEquals(medication.getCode(), drone.getMedications().get(0).getCode());
        assertEquals(medication.getWeight(), drone.getMedications().get(0).getWeight());
    }

    private Medication createMedication() {
       Medication medication = new Medication();
       medication.setCode("124");
       medication.setName("MED_12");
       medication.setWeight(0);
       return medication;
    }

    private Drone createDrone() {
        Drone drone = new Drone();
        drone.setSerialNumber("DR001");
        drone.setBatteryCapacity(100);
        drone.setState(DroneState.IDLE);

        return drone;
    }

    private MedicationDTO createMedicationDTO() {
        MedicationDTO medicationDTO = new MedicationDTO();
        medicationDTO.setCode("123");
        medicationDTO.setName(null);
        medicationDTO.setWeight(0);

        return medicationDTO;
    }

    public LoadDroneDTO createLoadDroneDTO() {
        LoadDroneDTO requestBody = new LoadDroneDTO();
        requestBody.setSerialNumber("DR001");
        requestBody.setMedications(Arrays.asList(
                new MedicationDTO("Paracetamol", "PARA_001", 100),
                new MedicationDTO("Panadol", "PAN_001", 200)
        ));
        return requestBody;
    }
}
